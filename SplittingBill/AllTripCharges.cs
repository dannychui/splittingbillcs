﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplittingBill
{
    class AllTripCharges
    {
        private int numberOfCampingTrips;
        private List<TripCharges> allCharges = new List<TripCharges>();

        public int getNumberOfCampingTrips()
        {
            return numberOfCampingTrips;
        }

        public void setNumberOfCampingTrips(int numberOfCampingTrips)
        {
            this.numberOfCampingTrips = numberOfCampingTrips;
        }

        public List<TripCharges> getAllCharges()
        {
            return allCharges;
        }
    }
}
