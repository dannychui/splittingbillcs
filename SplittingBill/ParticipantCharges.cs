﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplittingBill
{
    class ParticipantCharges
    {
        List<double> charges = new List<double>();
        double owingMoney;

        public List<double> getCharges()
        {
            return charges;
        }

        public double getParticipantTotalCharge()
        {

            double bd = 0.0;

            foreach (var charge in charges)
            {
                bd += charge;
            }

            return bd;
        }

        public double getOwingMoney()
        {
            return owingMoney;
        }

        public void setOwingMoney(double owingMoney)
        {
            this.owingMoney = owingMoney;
        }
    }
}
