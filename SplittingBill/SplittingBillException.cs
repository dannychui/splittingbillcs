﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplittingBill
{
    class SplittingBillException : System.Exception
    {
        private String errorCode;
        private String errorMsg;

        public SplittingBillException(String errorCode, String errorMsg) : base()
        {
            this.errorCode = errorCode;
            this.errorMsg = errorMsg;
        }

        public String getErrorCode()
        {
            return errorCode;
        }

        public String getErrorMsg()
        {
            return errorMsg;
        }
    }
}
