﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplittingBill
{

    /// <summary>
    /// 
    /// This class splits bills among partipicants for each trip.
    /// 
    /// Usage: From command window, go to the folder where SplittingBill.exe and the input file is located, enter: 
    ///
    ///     SplittingBill inputFilename
    /// 
    /// The output filename is the original input filename + “.out”.  For example, if the input file was expenses.txt then the output file would be expenses.txt.out.
    /// 
    /// </summary>
    public class SplittingBill
    {
        public static void Main(string[] args)
        {
            SplittingBill splittingBill = new SplittingBill();
            splittingBill.splitBill(args);
        }

        /// <summary>
        /// This me thod is the starting point to split the bills.
        /// </summary>
        /// <param name="args">The first item in the array contains the input filenam. Other items, if any, are ignored.</param>
        private void splitBill(String[] args)
        {
            try
            {
                String filename = validateCommandLineParameters(args);
                deleteOutputFile(filename + ".out");
                AllTripCharges allTripCharges = parseInputFile(filename);
                calculateAllTripOwingMoney(allTripCharges);
                outputAllOwingMoney(allTripCharges, filename + ".out");
            }
            catch (SplittingBillException sbe)
            {
                // already handled. 
            }
            catch (System.Exception e)
            {
                Console.WriteLine("SplittingBill entercounters a unexpected error.");
                Console.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// Delets the existing output file.
        /// </summary>
        /// <param name="filename">The output filename.</param>
        private void deleteOutputFile(String filename)
        {
     
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }

        private String validateCommandLineParameters(String[] args)
        {

            String filename = null;

            if (args.Length == 1)
            {
                filename = args[0];
            }
            else if (args.Length == 0)
            {
                usage();
                throw new SplittingBillException("SB-1", "Input filename not provided.");
            }
            else
            {
               Console.WriteLine("Warning: extra commandline parameters are ignored.");
                filename = args[0];
            }

            return filename;
        }

        private void usage()
        {
           Console.WriteLine("Usage: SplittingBill inputFilename");
        }

        /// <summary>
        /// Parses the input file and reads all charges for all trips.
        /// </summary>
        /// <param name="filename">The input filename.</param>
        /// <returns>AllTripCharges object that tracks charges for all trips.</returns>
        private AllTripCharges parseInputFile(String filename)
        {
            AllTripCharges allTripCharges = new AllTripCharges();
            int previousLineNum = 0;
            int currentLineNum = 0;

            using (StreamReader sr = File.OpenText(filename))
            {
                try
                {
                    TripCharges tripCharges = new TripCharges();

                    while ((currentLineNum = readNextTrip(sr, tripCharges, previousLineNum)) > (previousLineNum + 1))
                    {
                        previousLineNum = currentLineNum;
                        allTripCharges.getAllCharges().Add(tripCharges);
                        tripCharges = new TripCharges();
                    }
                }
                catch (FileNotFoundException fnfe)
                {
                    handleError("SB-2", "Input file is not found: " + filename);
                }
                catch (IOException ioe)
                {
                    handleError("SB-3", "Not able to read input file: " + filename);
                }
                catch (FormatException nfe)
                {
                    handleError("SB-4", "A number is expected at Line " + (previousLineNum + 1));
                }
            }

            return allTripCharges;
        }

        /// <summary>
        /// Reads charges for all participants for the trip.
        /// </summary>
        /// <param name="sr">StreamReader object for reading the input file.</param>
        /// <param name="tripCharges">TripCharges object that tracks charges for all participants for the trip.</param>
        /// <param name="previousLineNum">The number of the input file lines that have been read so far.</param>
        /// <returns>The number of lines that have been read.</returns>
        private int readNextTrip(StreamReader sr, TripCharges tripCharges, int previousLineNum)
        {


            String line = null;
            int participantNum = 0;
            int currentLineNum = previousLineNum;
	    
	        if ((line = sr.ReadLine()) != null)
            {
	    	    participantNum = Int32.Parse(line);
	    	    currentLineNum = ++previousLineNum;
	    	
	    	    if (participantNum > 0 )
                {
	    		
	    		    for (int i = 0; i<participantNum; i++)
                    {
	    			    ParticipantCharges charges = new ParticipantCharges();
                        currentLineNum = readNextParticipantCharges(sr, charges, previousLineNum);

                        // Here currentLineNum must be larger then previousLineNum. Otherwise, exception has been thrown already.
                        previousLineNum = currentLineNum;
    				    tripCharges.getCharges().Add(charges);
                    }
                }
                else if (participantNum == 0)
                {
	    		    // end of trips
	    		    // handle the case where the input file has only one line with number zero.
	    		    if (previousLineNum == 1)
                    {
	    			    handleError("SB-6", "The number of participants must be a positive number - Line " + previousLineNum);
	    		    }
	    	    }
                else
                {
	    		    handleError("SB-6", "The number of participants must be a positive number - Line " + previousLineNum);
	    	    }
	        }
            else if (previousLineNum > 0)
            {
	    	    handleError("SB-11", "The end of trips should be followed by 0 in its own line - Line " + (previousLineNum + 1));
	        }
            else
            {
	    	    //empty input file
	    	    handleError("SB-5", "The input file is empty.");
	        }
	    
	        return currentLineNum;
	    }

        /// <summary>
        /// Reads the charges for the next participant for the trip.
        /// </summary>
        /// <param name="sr">StreamReader object for reading the input file.</param>
        /// <param name="charges">ParticipantCharges object that tracks all charges for the participant.</param>
        /// <param name="previousLineNum">The number of the input file lines that have been read so far.</param>
        /// <returns>The number of lines that have been read.</returns>
	    private int readNextParticipantCharges(StreamReader sr, ParticipantCharges charges, int previousLineNum)
        {
		
		    String line = null;
            int chargesNum = 0;
            int currentLineNum = previousLineNum;
	    
	        if ((line = sr.ReadLine()) != null)
            {
	    	    chargesNum = Int32.Parse(line);
	    	    currentLineNum = ++previousLineNum;
	    	
	    	    if (chargesNum > 0 )
                {
	    		    for (int i = 0; i < chargesNum; i++)
                    {
	    			    double charge = readNextCharge(sr, previousLineNum);
                        currentLineNum = ++previousLineNum;

    				    charges.getCharges().Add(charge);
	    		    }
	    	    }
                else
                {
	    		    handleError("SB-7", "The number of charges must be a positive number - Line " + currentLineNum);
	    	    }
	        }
            else
            {
	    	    handleError("SB-8", "The number of charges is expected at Line " + currentLineNum);
	        }
	    
	        return currentLineNum;
	    }

        /// <summary>
        /// Reads the next charge item for the participant for the trip.
        /// </summary>
        /// <param name="sr">StreamReader object for reading the input file.</param>
        /// <param name="previousLineNum">The number of the input file lines that have been read so far.</param>
        /// <returns>The charge</returns>
	    private double readNextCharge(StreamReader sr, int previousLineNum)
        {
            String line = null;
            double charge = 0.0;
		
		    if ((line = sr.ReadLine()) != null)
            {
                double amount = Double.Parse(line);

                if (amount > 0)
                {
                    charge = amount;
                }
                else
                {
                    handleError("SB-10", "A positive amount is expected at Line " + (previousLineNum + 1));
                }
            }
            else
            {
                handleError("SB-9", "A charge is expected at Line " + (previousLineNum + 1));
            }
		
		    return charge;
        }

        private void handleError(String errCode, String errMsg)
        {
            Console.WriteLine(errMsg);
            throw new SplittingBillException(errCode, errMsg);
        }

        private void calculateAllTripOwingMoney(AllTripCharges allTripCharges)
        {

            foreach (var tripCharges in allTripCharges.getAllCharges())
            {
                tripCharges.calculateparticipantOwingMoney();
            }
        }

        /// <summary>
        /// Generates output file.
        /// </summary>
        /// <param name="allTripCharges">AllTripCharges object that tracks charges for all trips.</param>
        /// <param name="filename">The output filename.</param>
        private void outputAllOwingMoney(AllTripCharges allTripCharges, String filename)
        {

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                bool firstTrip = true;
                
                foreach (var tripCharges in allTripCharges.getAllCharges())
                {
                    if (!firstTrip)
                    {
                        file.WriteLine();
                    } else
                    {
                        firstTrip = false;
                    }

                    foreach (var participantCharges in tripCharges.getCharges())
                    {
                        double d = Math.Round(participantCharges.getOwingMoney(), 2);
                        String formattedAmount = "$" + Math.Abs(d);
                        formattedAmount = (d >= 0) ? formattedAmount : "(" + formattedAmount + ")";
                        file.WriteLine(formattedAmount);
                    }
                }
            }
	    }
    }
}
