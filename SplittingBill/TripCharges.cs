﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplittingBill
{
    class TripCharges
    {
        private List<ParticipantCharges> charges = new List<ParticipantCharges>();

        public int getNumberOfParticipants()
        {
            return charges.Count;
        }

        public List<ParticipantCharges> getCharges()
        {
            return charges;
        }

        public void calculateparticipantOwingMoney()
        {

            double totalCharge = 0.0;

            // total trip charge
            foreach (var participantCHarges in charges)
            {
                totalCharge += participantCHarges.getParticipantTotalCharge();
            }

            // trip average charge per participant
            double average = Math.Round(totalCharge / getNumberOfParticipants(), 2);

            // per participant owing money
            foreach (var participantCHarges in charges)
            {
                double owing = average - participantCHarges.getParticipantTotalCharge();
                participantCHarges.setOwingMoney(owing);
            }
        }
    }
}
