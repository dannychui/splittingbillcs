using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace SplittingBillTests
{
    [TestClass]
    public class UnitTestSplittingBill
    {
        static string projectDirectory = System.IO.Directory.GetParent(System.Environment.CurrentDirectory).Parent.Parent.FullName;
        static string testdatafolder = projectDirectory + "\\testdata\\";

        [ClassInitialize]
        public static void setup(TestContext ctx)
        {
        }

        [TestMethod]
        public void HappyScenario()
        {
            string inputFile = testdatafolder + "expenses.txt";
            string outputFile = testdatafolder + "expenses.txt.out";
            string expectedoutputFile = testdatafolder + "expenses.txt.expected";

            SplittingBill.SplittingBill.Main(new string[]{inputFile});
            bool filesAreEqual = File.ReadAllBytes(outputFile).SequenceEqual(File.ReadAllBytes(expectedoutputFile));
            Assert.AreEqual(filesAreEqual, true);
        }
    }
}
